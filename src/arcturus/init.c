/*
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/reboot.h>
#include <termios.h>
#include <lzma.h>
#include <zlib.h>
#include <dirent.h>

// For raspbian kernels set these
//#define SKIP_INITRD
//#define USE_TMPFS

#define ATTR_UNUSED __attribute__((__unused__))

#define STREQ(x,y) (strcmp(x,y) == 0)
#define STRNEQ(x,y) (strcmp(x,y) != 0)

static void print_uptime (void);
static void insmod (const char *filename, const char *params);
static void set_debug(void);
static const char *argv0;

static int debug = 0;
static char line[1024];

static void exit_poweroff(void) __attribute__((noreturn));

static void exit_poweroff(void)
{
    sync();
    fprintf(stderr, "Powering off in 15 seconds\n");
    sleep(15);
    reboot(RB_POWER_OFF);
    perror("reboot");
    abort();
}

static void sig_child(int signum ATTR_UNUSED)
{
    int status;
    while (1) {
        int ret = waitpid(-1, &status, WNOHANG);
        if (ret == -1 || ret == 0)
            break;
    }
}

static void
mount_mkdir(const char *dir, int mode);

static void
mount_mkparent(const char *dir, int mode)
{
    char *tmp = strrchr(dir, '/');
    if (tmp && tmp != dir) {
        char *parent = strndup(dir, tmp - dir);
        mount_mkdir(parent, mode);
        free(parent);
    }
}

static void
mount_mkdir(const char *dir, int mode)
{
    mount_mkparent(dir, 0755);

    if (debug)
        fprintf(stderr, "%s: %s: %s (mode=%o)\n", argv0, __func__, dir, mode);

    if (mkdir(dir, mode) < 0) {
        if (errno != EEXIST) {
            fprintf(stderr, "%s: %s: cannot make directory %s: %s\n",
                    argv0, __func__, dir, strerror(errno));
            exit_poweroff();
        }
    }
}

static void
mount_other(const char *dst, const char *type, int mode)
{
    if (debug)
        fprintf(stderr, "%s: %s: %s (type=%s)\n",
                argv0, __func__, dst, type);

    mount_mkdir(dst, mode);

    if (mount("none", dst, type, 0, "") < 0) {
        fprintf(stderr, "%s: %s: cannot mount %s on %s (%s): %s\n",
                argv0, __func__, type, dst, type, strerror(errno));
        exit_poweroff();
    }

    if (chmod(dst, mode) < 0) {
        fprintf(stderr, "%s: %s: cannot set directory mode %s: %s\n",
                argv0, __func__, dst, strerror(errno));
        exit_poweroff();
    }
}

int
main(int argc ATTR_UNUSED, char **argv)
{
    const char *args[50];
    int narg = 0;

    argv0 = argv[0];

    if (getpid() != 1) {
        fprintf(stderr, "%s: %s: must be run as the 'init' program of a Raspberry Pi Zero\n",
                argv0, __func__);
        exit(EXIT_FAILURE);
    }

    set_debug();

    if (debug)
        fprintf(stderr, "%s: %s: ext2 mini initrd starting up\n",
                argv0, __func__);

    mount_other("/sys", "sysfs", 0755);
    mount_other("/proc", "proc", 0755);
#ifdef USE_TMPFS
    mount_other("/dev", "tmpfs", 0755);

#define MKNOD(file, mode, dev)                                          \
    do {                                                                \
        if (mknod(file, mode, dev) < 0) {                               \
            fprintf(stderr, "libvirt-sandbox-init-qemu: %s: cannot make dev %s %llu: %s\n", \
                    __func__, file, (unsigned long long)dev, strerror(errno)); \
            exit_poweroff();                                            \
        }                                                               \
    } while (0)

    umask(0000);
    MKNOD("/dev/null", S_IFCHR |0666, makedev(1, 3));
    MKNOD("/dev/zero", S_IFCHR |0666, makedev(1, 5));
    MKNOD("/dev/full", S_IFCHR |0666, makedev(1, 7));
    MKNOD("/dev/random", S_IFCHR |0666, makedev(1, 8));
    MKNOD("/dev/urandom", S_IFCHR |0666, makedev(1, 9));
    MKNOD("/dev/console", S_IFCHR |0700, makedev(5, 1));
    MKNOD("/dev/tty", S_IFCHR |0700, makedev(5, 0));
    MKNOD("/dev/tty0", S_IFCHR |0700, makedev(4, 0));
    MKNOD("/dev/tty1", S_IFCHR |0700, makedev(4, 1));
    MKNOD("/dev/tty2", S_IFCHR |0700, makedev(4, 2));
    MKNOD("/dev/ttyS0", S_IFCHR |0700, makedev(4, 64));
    MKNOD("/dev/ttyS1", S_IFCHR |0700, makedev(4, 65));
    MKNOD("/dev/ttyS2", S_IFCHR |0700, makedev(4, 66));
    MKNOD("/dev/ttyS3", S_IFCHR |0700, makedev(4, 67));
    MKNOD("/dev/hvc0", S_IFCHR |0700, makedev(229, 0));
    MKNOD("/dev/hvc1", S_IFCHR |0700, makedev(229, 1));
    MKNOD("/dev/hvc2", S_IFCHR |0700, makedev(229, 2));
    MKNOD("/dev/fb", S_IFCHR |0700, makedev(29, 0));
    MKNOD("/dev/fb0", S_IFCHR |0700, makedev(29, 0));
    MKNOD("/dev/mem", S_IFCHR |0600, makedev(1, 1));
    MKNOD("/dev/rtc", S_IFCHR |0700, makedev(254, 0));
    MKNOD("/dev/rtc0", S_IFCHR |0700, makedev(254, 0));
    MKNOD("/dev/ptmx", S_IFCHR |0777, makedev(5, 2));
    MKNOD("/dev/video0", S_IFCHR |0700, makedev(81, 0));
    MKNOD("/dev/mmcblk0", S_IFCHR |0700, makedev(179, 0));
    MKNOD("/dev/mmcblk0p1", S_IFCHR |0700, makedev(179, 1));
    MKNOD("/dev/mmcblk0p2", S_IFCHR |0700, makedev(179, 2));
#else
    mount_other("/dev", "devtmpfs", 0755);
#endif

    umask(0022);

    if (symlink("/proc/self/fd", "/dev/fd") < 0) {
        fprintf(stderr, "%s: %s: failed to create /dev/fd symlink: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }

    FILE *fp = fopen("/modules.txt", "r");
    if (fp == NULL) {
        fprintf(stderr, "%s: %s: cannot open /modules.txt: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }
    while (fgets(line, sizeof line, fp)) {
        size_t n = strlen(line);
        if (n > 0 && line[n-1] == '\n')
            line[--n] = '\0';
        char *param = strchr(line, ' ');
        if (param) {
            *param = '\0';
            param++;
        }
        insmod(line, param);
    }
    fclose(fp);

#ifndef SKIP_INITRD
    mount_mkdir("/data", 0755);

    if (mount("/dev/mmcblk0p2", "/data", "ext4", MS_NOSUID|MS_NODEV|MS_NOEXEC, NULL) < 0) {
        fprintf(stderr, "%s: %s: cannot mount /dev/mmcblk0p2 on /data (ext4): %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }
#endif

    if (debug)
        fprintf(stderr, "%s: %s: preparing to launch app\n",
                argv0, __func__);
    /* Run /init from ext2 filesystem. */
    print_uptime();

    signal(SIGCHLD, sig_child);

    memset(&args, 0, sizeof(args));

    args[narg++] = "/app";
    if (debug && 0)
        args[narg++] = "-d";

    if (debug)
        fprintf(stderr,
                "%s: %s: Running app %s\n",
                argv0, __func__, args[0]);
    execv(args[0], (char**)args);
    fprintf(stderr, "%s: %s: cannot execute %s: %s\n",
            argv0, __func__, args[0], strerror(errno));
    exit_poweroff();
}

/* This is a function exported by glibc, but not in any header :-) */
extern long init_module(const void *mem, unsigned long len, const char *args);

#define READ_SIZE (1024 * 16)

static char *readall(const char *filename, size_t *len)
{
    char *data = NULL, *tmp;
    int fd;
    size_t capacity;
    size_t offset;
    ssize_t got;

    *len = capacity = offset = 0;

    if ((fd = open(filename, O_RDONLY)) < 0) {
        fprintf(stderr, "%s: %s: cannot open %s\n",
                argv0, __func__, filename);
        exit_poweroff();
    }

    for (;;) {
        if ((capacity - offset) < 1024) {
            if (!(tmp = realloc(data, capacity + 2048))) {
                fprintf(stderr, "%s: %s: out of memory\n",
                        argv0, __func__);
                exit_poweroff();
            }
            data = tmp;
            capacity += 2048;
        }

        if ((got = read(fd, data + offset, capacity - offset)) < 0) {
            fprintf(stderr, "%s: %s: error reading %s: %s\n",
                    argv0, __func__, filename, strerror(errno));
            exit_poweroff();
        }
        if (got == 0)
            break;

        offset += got;
    }
    *len = offset;
    close(fd);
    return data;
}


static int
has_suffix(const char *filename, const char *ext)
{
    char *offset = strstr(filename, ext);
    return  (offset &&
             offset[strlen(ext)] == '\0');
}

static char *
load_module_file_lzma(const char *filename, size_t *len)
{
    lzma_stream st = LZMA_STREAM_INIT;
    char *xzdata;
    size_t xzlen;
    char *data;
    lzma_ret ret;

    *len = 0;

    if (debug)
        fprintf(stderr, "%s: %s: %s\n",
                argv0, __func__, filename);

    if ((ret = lzma_stream_decoder(&st, UINT64_MAX,
                                   LZMA_CONCATENATED)) != LZMA_OK) {
        fprintf(stderr, "%s: %s: %s: lzma init failure: %d\n",
                argv0, __func__, filename, ret);
        exit_poweroff();
    }
    xzdata = readall(filename, &xzlen);

    st.next_in = (unsigned char *)xzdata;
    st.avail_in = xzlen;

    *len = 32 * 1024;
    if (!(data = malloc(*len))) {
        fprintf(stderr, "%s: %s: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }

    st.next_out = (unsigned char *)data;
    st.avail_out = *len;

    do {
        ret = lzma_code(&st, LZMA_FINISH);
        if (st.avail_out == 0) {
            size_t used = *len;
            *len += (32 * 1024);
            if (!(data = realloc(data, *len))) {
                fprintf(stderr, "%s: %s: %s\n",
                        argv0, __func__, strerror(errno));
                exit_poweroff();
            }
            st.next_out = (unsigned char *)data + used;
            st.avail_out = *len - used;
        }
        if (ret != LZMA_OK && ret != LZMA_STREAM_END) {
            fprintf(stderr, "%s: %s: %s: lzma decode failure: %d\n",
                    argv0, __func__, filename, ret);
            exit_poweroff();
        }
    } while (ret != LZMA_STREAM_END);
    lzma_end(&st);
    free(xzdata);
    return data;
}

static char *
load_module_file_zlib(const char *filename, size_t *len)
{
    gzFile fp;
    char *data;
    unsigned int avail;
    size_t total;
    int got;

    if (debug)
        fprintf(stderr, "%s: %s: %s\n", argv0, __func__, filename);

    if (!(fp = gzopen(filename, "rb"))) {
        fprintf(stderr, "%s: %s: %s: gzopen failure\n",
                argv0, __func__, filename);
        exit_poweroff();
    }

    *len = 32 * 1024;
    if (!(data = malloc(*len))) {
        fprintf(stderr, "%s: %s: %s\n", argv0, __func__, strerror(errno));
        exit_poweroff();
    }

    avail = *len;
    total = 0;

    do {
        got = gzread(fp, data + total, avail);

        if (got < 0) {
            fprintf(stderr, "%s: %s: %s: gzread failure\n",
                    argv0, __func__, filename);
            exit_poweroff();
        }

        total += got;
        if (total >= *len) {
            *len += (32 * 1024);
            if (!(data = realloc(data, *len))) {
                fprintf(stderr,
                        "%s: %s: %s\n", argv0, __func__, strerror(errno));
                exit_poweroff();
            }
            avail = *len - total;
        }
    } while (got > 0);

    gzclose(fp);
    return data;
}

static char *
load_module_file_raw(const char *filename, size_t *len)
{
    if (debug)
        fprintf(stderr, "%s: %s: %s\n", argv0, __func__, filename);
    return readall(filename, len);
}

static char *
load_module_file(const char *filename, size_t *len)
{
    if (has_suffix(filename, ".ko.xz"))
        return load_module_file_lzma(filename, len);
    else if (has_suffix(filename, ".ko.gz"))
        return load_module_file_zlib(filename, len);
    else
        return load_module_file_raw(filename, len);
}


static void
insmod(const char *filename, const char *params)
{
    char *data;
    size_t len;
    if (debug)
        fprintf(stderr, "Load %s\n", filename);

    data = load_module_file(filename, &len);

    if (init_module(data, (unsigned long)len, params ? params : "") < 0) {
        const char *msg;
        switch (errno) {
        case ENOEXEC:
            msg = "Invalid module format";
            break;
        case ENOENT:
            msg = "Unknown symbol in module";
            break;
        case ESRCH:
            msg = "Module has wrong symbol version";
            break;
        case EINVAL:
            msg = "Invalid parameters";
            break;
        default:
            msg = strerror(errno);
        }
        fprintf(stderr, "%s: %s: error loading %s: %s\n",
                argv0, __func__, filename, msg);
        exit_poweroff();
    }
}

/* Print contents of /proc/uptime. */
static void
print_uptime(void)
{
    if (!debug)
        return;
    FILE *fp = fopen("/proc/uptime", "r");
    if (fp == NULL) {
        fprintf(stderr, "%s: %s: cannot open /proc/uptime: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }

    if (!fgets(line, sizeof line, fp)) {
        fprintf(stderr, "%s: %s: cannot read /proc/uptime: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }
    fclose(fp);

    fprintf(stderr, "%s: %s: uptime: %s", argv0, __func__, line);
}


static void set_debug(void)
{
    if (mkdir("/proc", 0755) < 0 && errno != EEXIST) {
        fprintf(stderr, "%s: %s: cannot mkdir /proc: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }
    if (mount("none", "/proc", "proc", 0, "") < 0) {
        fprintf(stderr, "%s: %s: cannot mount /proc: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }
    FILE *fp = fopen("/proc/cmdline", "r");

    if (fp && fgets(line, sizeof line, fp)) {
        if (strstr(line, "debug"))
            debug=1;
    }
    if (fp)
        fclose(fp);
    if (umount("/proc") < 0) {
        fprintf(stderr, "%s: %s: cannot unmount /proc: %s\n",
                argv0, __func__, strerror(errno));
        exit_poweroff();
    }
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
