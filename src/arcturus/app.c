/*
 * Copyright (C) 2016 Daniel P. Berrange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/reboot.h>
#include <sys/mount.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <jpeglib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <time.h>
#include <getopt.h>

struct Config {
    bool disableGPIO;
    bool debug;
    char *logfile;
    int pinShutterLED;
    int pinPowerLED;
    int pinShutterButton;
    int captureDelay;
    char *device;
};

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000)
#define GPIO_BLOCK_SIZE (4*1024)

#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))

#define GPIO_SET(pin) (*(gpio+7) = 1 << pin)
#define GPIO_CLR(pin) (*(gpio+10) = 1 << pin)
#define GPIO_GET(pin) (*(gpio+13) & (1 << pin))

#define SHUTTER_IS_PRESSED GPIO_GET(SHUTTER_GPIO) == 0

#define GPIO_PUD_OFF 0
#define GPIO_PUD_DOWN 1
#define GPIO_PUD_UP 2

#define GPIO_PUD *(gpio+37)
#define GPIO_PUDCLK *(gpio+38)


static FILE *logger;
static bool debugOn = false;

void debug(const char *msg, ...)
{
    va_list args;
    if (!debugOn)
        return;
    va_start(args, msg);
    vfprintf(logger, msg, args);
    fflush(logger);
    va_end(args);
}


volatile unsigned *gpio_init_mmap(void)
{
    int mem_fd;
    void *gpio_map;

    debug("Opening /dev/mem\n");

    /* open /dev/mem */
    if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
        perror("can't open /dev/mem");
        return NULL;
    }

    /* mmap GPIO */
    debug("Mapping GPIO\n");
    gpio_map = mmap(NULL,
                    GPIO_BLOCK_SIZE,
                    PROT_READ|PROT_WRITE,
                    MAP_SHARED,
                    mem_fd,
                    GPIO_BASE);

    close(mem_fd);

    if (gpio_map == MAP_FAILED) {
        perror("mmap error");
        return NULL;
    }

    return (volatile unsigned *)gpio_map;
}


void delayMicroseconds(unsigned int howLong)
{
    struct timeval start, delta, end;

    gettimeofday(&start, NULL);
    delta.tv_sec = howLong / 1000000;
    delta.tv_usec = howLong % 1000000;
    timeradd(&start, &delta, &end);

    while (timercmp(&start, &end, <))
        gettimeofday(&start, NULL);
}


static void gpio_init_leds(struct Config *cfg, volatile unsigned *gpio)
{
    INP_GPIO(cfg->pinPowerLED);
    OUT_GPIO(cfg->pinPowerLED);

    INP_GPIO(cfg->pinShutterLED);
    OUT_GPIO(cfg->pinShutterLED);

    if (cfg->pinShutterButton != 0) {
        INP_GPIO(cfg->pinShutterButton);

        GPIO_PUD = GPIO_PUD_UP;
        delayMicroseconds(5);
        GPIO_PUDCLK = (1 << cfg->pinShutterButton);
        delayMicroseconds(5);

        GPIO_PUD = 0;
        delayMicroseconds(5);
        GPIO_PUDCLK = 0;
        delayMicroseconds(5);
    }
}


static char *webcam_next_filename(const char *basedir, int *dir_index, int *file_index)
{
    size_t dir, file;
    char *path;

    debug("Determining next filename %s %d %d\n", basedir, *dir_index, *file_index);
    if (mkdir(basedir, 0777) < 0 &&
        errno != EEXIST) {
        perror("mkdir");
        return NULL;
    }
    for (dir = *dir_index ; dir < 1000; dir++) {
        if (asprintf(&path, "%s/%03zdPINHOLE", basedir, dir) < 0) {
            perror("asprintf");
            return NULL;
        }

        if (mkdir(path, 0777) < 0 &&
            errno != EEXIST) {
            free(path);
            perror("mkdir");
            return NULL;
        }
        free(path);

        for (file = *file_index ; file < 10000 ; file++) {
            if (asprintf(&path, "%s/%03zdPINHOLE/DSC_%04zd.JPG",
                         basedir, dir, file) < 0) {
                perror("asprintf");
                return NULL;
            }

            if (access(path, F_OK) == -1 &&
                errno == ENOENT) {
                *dir_index = dir;
                *file_index = file + 1;
                if (*file_index == 10000) {
                    (*dir_index)++;
                    *file_index = 1;
                }
                return path;
            }
            free(path);
        }
    }
    errno = ENOSPC;
    perror("next filename");
    return NULL;
}


static int webcam_init_format(int fd, int *pix_format, int *width, int *height)
{
    int pick_area = 0, pick_width, pick_height, pick_format = 0;
    struct v4l2_capability caps = {};
    struct v4l2_fmtdesc fmtdesc = {0};
    struct v4l2_format fmt = {0};
    char fourcc[5] = {0};

    if (ioctl(fd, VIDIOC_QUERYCAP, &caps) < 0) {
        perror("Querying Capabilities");
        return -1;
    }

    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    /* Looking for mjpeg/jpeg/yuyv422 format with largest size */
    while (ioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc) == 0) {
        struct v4l2_frmsizeenum framesize = {0};
        int matched = 0;

        strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
        debug("Format %c%c%c%c\n", fourcc[0], fourcc[1], fourcc[2], fourcc[3]);
        fmtdesc.index++;

        if (fmtdesc.pixelformat == V4L2_PIX_FMT_JPEG ||
            fmtdesc.pixelformat == V4L2_PIX_FMT_MJPEG ||
            fmtdesc.pixelformat == V4L2_PIX_FMT_YUYV)
            matched = 1;

        framesize.pixel_format = fmtdesc.pixelformat;
        while (ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &framesize) == 0) {
            switch (framesize.type) {
            case V4L2_FRMSIZE_TYPE_DISCRETE:
                debug(" Discrete frame %dx%d\n",
                      framesize.discrete.width, framesize.discrete.height);
                if ((framesize.discrete.width *
                     framesize.discrete.height) > pick_area &&
                    matched) {
                    pick_width = framesize.discrete.width;
                    pick_height = framesize.discrete.height;
                    pick_area = pick_width * pick_height;
                    pick_format = fmtdesc.pixelformat;
                }
                break;
            case V4L2_FRMSIZE_TYPE_STEPWISE:
            case V4L2_FRMSIZE_TYPE_CONTINUOUS:
                debug(" Stepwise frame %dx%d\n",
                      framesize.stepwise.max_width, framesize.stepwise.max_height);
                if (matched) {
                    pick_width = framesize.stepwise.max_width;
                    pick_height = framesize.stepwise.max_height;
                    pick_area = pick_width * pick_height;
                    pick_format = fmtdesc.pixelformat;
                }
                break;

            default:
                break;
            }

            if (framesize.type != V4L2_FRMSIZE_TYPE_DISCRETE)
                break;
            else
                framesize.index++;
        }
    }

    if (!pick_format) {
        fprintf(stderr, "Cannot find mjpeg/jpeg formats\n");
        return -1;
    }

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width = pick_width;
    fmt.fmt.pix.height = pick_height;
    fmt.fmt.pix.pixelformat = pick_format;
    fmt.fmt.pix.field = V4L2_FIELD_NONE;

    strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);

    debug("Pick format %c%c%c%c\n", fourcc[0], fourcc[1], fourcc[2], fourcc[3]);
    debug("Pick size %dx%d\n", pick_width, pick_height);

    if (ioctl(fd, VIDIOC_S_FMT, &fmt) < 0) {
        perror("Setting Pixel Format");
        return -1;
    }

    *pix_format = pick_format;
    *width = pick_width;
    *height = pick_height;

    return 0;
}

void *webcam_init_mmap(int fd)
{
    struct v4l2_requestbuffers req = {0};
    struct v4l2_buffer buf = {0};
    void *buffer;

    req.count = 1;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (ioctl(fd, VIDIOC_REQBUFS, &req) < 0) {
        perror("Requesting Buffer");
        return NULL;
    }

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;

    if (ioctl(fd, VIDIOC_QUERYBUF, &buf) < 0) {
        perror("Querying Buffer");
        return NULL;
    }

    if ((buffer = mmap(NULL, buf.length,
                       PROT_READ | PROT_WRITE,
                       MAP_SHARED,
                       fd, buf.m.offset)) == MAP_FAILED) {
        perror("mmap");
        return NULL;
    }

    return buffer;
}

static int start_streaming(int fd)
{
    struct v4l2_buffer buf = {0};
    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    buf.type = type;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;

    debug("Queuing buffer\n");
    if (ioctl(fd, VIDIOC_QBUF, &buf) < 0) {
        perror("Query Buffer");
        return -1;
    }

    debug("Turn on streaming\n");
    if (ioctl(fd, VIDIOC_STREAMON, &type) < 0) {
        perror("Start Capture");
        return -1;
    }

    return 0;
}

static int stop_streaming(int fd)
{
    struct v4l2_buffer buf = {0};
    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    buf.type = type;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;

    debug("Turn off streaming\n");
    if (ioctl(fd, VIDIOC_STREAMOFF, &buf.type) < 0) {
        perror("Stop Capture");
        return -1;
    }
    return 0;
}


struct error_handler_data {
        struct jpeg_error_mgr pub;
        sigjmp_buf setjmp_buffer;
};

static void
fatal_error_handler (j_common_ptr cinfo)
{
        /* FIXME:
         * We should somehow signal what error occurred to the caller so the
         * caller can handle the error message */
        struct error_handler_data *errmgr;

        errmgr = (struct error_handler_data *) cinfo->err;
        cinfo->err->output_message (cinfo);
        siglongjmp (errmgr->setjmp_buffer, 1);

        /* incase the jmp buf isn't initted? */
        exit(1);
}

int webcam_save_image_yuyv(const char *path, char *buffer, size_t len, int width, int height)
{
    struct jpeg_compress_struct cinfo = {0};
    struct error_handler_data jerr;
    volatile FILE *fh;

    if (!(fh = fopen(path, "wb"))) {
        perror("open");
        return -1;
    }

    /* setup error handler */
    cinfo.err = jpeg_std_error (&jerr.pub);
    jerr.pub.error_exit = fatal_error_handler;

    if (sigsetjmp (jerr.setjmp_buffer, 1)) {
        /* Whoops there was a jpeg error */

        jpeg_destroy_compress(&cinfo);
        return -1;
    }

    jpeg_create_compress(&cinfo);

    jpeg_stdio_dest (&cinfo, (FILE *)fh);

    cinfo.image_width = width & -1;
    cinfo.image_height = height & -1;
    cinfo.input_components = 3;
    cinfo.in_color_space = JCS_YCbCr;
    jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo, 97, TRUE);
    jpeg_start_compress(&cinfo, TRUE);
    unsigned char *jbuf = malloc(width * 3);
    while (cinfo.next_scanline < height) {
        unsigned char *jrow[1];
        for (int i = 0; i < cinfo.image_width; i += 2) {
            jbuf[i*3] = buffer[i*2];
            jbuf[i*3+1] = buffer[i*2+1];
            jbuf[i*3+2] = buffer[i*2+3];
            jbuf[i*3+3] = buffer[i*2+2];
            jbuf[i*3+4] = buffer[i*2+1];
            jbuf[i*3+5] = buffer[i*2+3];
        }
        jrow[0] = jbuf;
        buffer += width * 2;
        jpeg_write_scanlines(&cinfo, jrow, 1);
    }
    jpeg_finish_compress(&cinfo);
    free(jbuf);
    return 0;
}


int webcam_save_image_jpeg(const char *path, void *buffer, size_t len)
{
    int ret = -1;
    int imgfd;

    if ((imgfd = open(path, O_WRONLY|O_CREAT|O_TRUNC, 0644)) < 0) {
        perror("open");
        return -1;
    }
    if (write(imgfd, buffer, len) != len) {
        errno = EIO;
        perror("write");
        goto cleanup;
    }
    if (fsync(imgfd) < 0) {
        perror("sync");
        goto cleanup;
    }
    ret = 0;
 cleanup:
    close(imgfd);
    return ret;
}


int webcam_save_image(void *buffer, size_t len, int pix_format, int width, int height, int *dir_index, int *file_index)
{
    char *path;
    int ret;

    if (!(path = webcam_next_filename("/data/DCIM", dir_index, file_index))) {
        return -1;
    }

    debug("Saving image %s\n", path);
    switch (pix_format) {
    case V4L2_PIX_FMT_YUYV:
        ret = webcam_save_image_yuyv(path, buffer, len, width, height);
        break;

    case V4L2_PIX_FMT_JPEG:
    case V4L2_PIX_FMT_MJPEG:
        ret = webcam_save_image_jpeg(path, buffer, len);
        break;

    default:
        fprintf(stderr, "unexpected format %d\n", pix_format);
        free(path);
        return -1;
    }
    sync();
    if (ret != -1) {
        debug("Saved %s\n", path);
    }
    free(path);
    return ret;
}

int webcam_capture_image(int fd, void *buffer, int pix_format, int width, int height, int *dir_index, int *file_index)
{
    int ret;
    struct v4l2_buffer buf = {0};
    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    buf.type = type;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;

    debug("Waiting for capture\n");
    if (ioctl(fd, VIDIOC_DQBUF, &buf) < 0) {
        perror("Retrieving Frame");
        return -1;
    }

    debug("Result flags %d\n", buf.flags);
    ret = webcam_save_image(buffer, buf.bytesused, pix_format, width, height,
                            dir_index, file_index);

    memset(&buf, 0, sizeof(buf));
    buf.type = type;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = 0;

    debug("Queuing buffer\n");
    if (ioctl(fd, VIDIOC_QBUF, &buf) < 0) {
        perror("Query Buffer");
        return -1;
    }

    return ret;
}

enum ConfigEntryType {
    CONFIG_TYPE_INT,
    CONFIG_TYPE_BOOL,
    CONFIG_TYPE_STRING,
};

struct ConfigEntry {
    const char *name;
    int type;
    void *val;
};


int load_config(struct Config *cfg, const char *filename)
{
    FILE *fh = fopen(filename, "r");
    char *line = NULL;
    size_t len = 0;
    char *sep, *val;
    struct ConfigEntry entries[] = {
        { "debug", CONFIG_TYPE_BOOL, &cfg->debug },
        { "log-file", CONFIG_TYPE_STRING, &cfg->logfile },
        { "disable-gpio", CONFIG_TYPE_BOOL, &cfg->disableGPIO },
        { "pin-power-led", CONFIG_TYPE_INT, &cfg->pinPowerLED },
        { "pin-shutter-led", CONFIG_TYPE_INT, &cfg->pinShutterLED },
        { "pin-shutter-button", CONFIG_TYPE_INT, &cfg->pinShutterButton },
        { "capture-delay", CONFIG_TYPE_INT, &cfg->captureDelay },
        { "device", CONFIG_TYPE_STRING, &cfg->device },
    };
    size_t i;

    if (!fh) {
        return 0;
    }

    while (getline(&line, &len, fh) > 0) {
        if (line[0] != '\n' &&
            line[0] != '#') {
            val = sep = strchr(line, '=');
            if (val) {
                bool missing = true;
                sep--;
                while (sep > line && *sep == ' ') {
                    *sep = '\0';
                    sep--;
                }
                val++;
                while (*val == ' ') {
                    val++;
                }

                for (i = 0; i < sizeof(entries)/sizeof(entries[0]); i++) {
                    if (strcmp(entries[i].name, line) == 0) {
                        missing = false;
                        switch (entries[i].type) {
                        case CONFIG_TYPE_BOOL:
                            if (strcmp(val, "1") == 0||
                                strcmp(val, "y") == 0)
                                *((bool *)entries[i].val) = true;
                            else
                                *((bool *)entries[i].val) = false;
                            break;

                        case CONFIG_TYPE_INT:
                            *((int *)entries[i].val) = atoi(val);
                            break;

                        case CONFIG_TYPE_STRING:
                            free(*(char **)entries[i].val);
                            *((char **)entries[i].val) = strdup(val);
                            break;
                        }

                        break;
                    }
                }

                if (missing) {
                    fprintf(stderr, "Unknown config entry '%s'\n", val);
                }
            }
        }

        free(line);
        line = NULL;
        len = 0;
    }

    fclose(fh);
    return 0;
}


int load_argv(struct Config *config, int argc, char **argv) {
    enum {
        OPT_DEBUG = 200,
        OPT_LOG_FILE,
        OPT_DISABLE_GPIO,
        OPT_PIN_POWER_LED,
        OPT_PIN_SHUTTER_LED,
        OPT_PIN_SHUTTER_BUTTON,
        OPT_CAPTURE_DELAY,
    };
    struct option opts[] = {
        { "debug", 0, NULL, OPT_DEBUG },
        { "log-file", 1, NULL, OPT_LOG_FILE },
        { "disable-gpio", 0, NULL, OPT_DISABLE_GPIO },
        { "pin-power-led", 1, NULL, OPT_PIN_POWER_LED },
        { "pin-shutter-led", 1, NULL, OPT_PIN_SHUTTER_LED },
        { "pin-shutter-button", 1, NULL, OPT_PIN_SHUTTER_BUTTON },
        { "capture-delay", 1, NULL, OPT_CAPTURE_DELAY },
        { "device", 1, NULL, 'd' },
    };

    while (1) {
        int c;
        c = getopt_long(argc, argv, "d:", opts, NULL);

        if (c == -1)
            break;

        switch (c) {
        case 'd':
            free(config->device);
            config->device = strdup(optarg);
            break;

        case OPT_DEBUG:
            config->debug = true;
            break;

        case OPT_LOG_FILE:
            free(config->logfile);
            config->logfile= strdup(optarg);
            break;

        case OPT_DISABLE_GPIO:
            config->disableGPIO = true;
            break;

        case OPT_PIN_POWER_LED:
            config->pinPowerLED = atoi(optarg);
            break;

        case OPT_PIN_SHUTTER_LED:
            config->pinShutterLED = atoi(optarg);
            break;

        case OPT_PIN_SHUTTER_BUTTON:
            config->pinShutterButton = atoi(optarg);
            break;

        case OPT_CAPTURE_DELAY:
            config->captureDelay = atoi(optarg);
            break;

        case '?':
            fprintf(stderr, "Unknown parameter\n");
            return -1;
        }
    }

    return 0;
}

int main(int argc, char **argv)
{
    int fd = -1;
    int i;
    bool toggle = false;
    volatile unsigned *gpio;
    void *buffer;
    int file_index = 1;
    int dir_index = 1;
    int pix_format;
    int width, height;
    int debounced = 0;
    struct Config cfg = {
        .debug = false,
        .logfile = NULL,
        .disableGPIO = false,
        .pinPowerLED = 18,
        .pinShutterLED = 17,
        .pinShutterButton = 0,
        .device = strdup("/dev/video0"),
        .captureDelay = 1,
    };

    if (load_argv(&cfg, argc, argv) < 0)
        exit(1);

    if (load_config(&cfg, "/app.cfg") < 0)
        exit(1);

    debugOn = cfg.debug;

    if (cfg.logfile) {
        logger = fopen(cfg.logfile, "w");
        if (!logger) {
            perror(cfg.logfile);
            exit(1);
        }
    } else {
        logger = stderr;
    }

    logger = stderr;
    debug("*********************************\n");
    debug("********** Arcturus *************\n");
    debug("*********************************\n");
    debug("\n");
    debug(" GPIO: %s\n", cfg.disableGPIO ? "off" : "on");
    debug("  - Pin Power LED %d\n", cfg.pinPowerLED);
    debug("  - Pin Shutter LED %d\n", cfg.pinShutterLED);
    debug("  - Pin Shutter Button %d\n", cfg.pinShutterButton);
    debug(" Device: %s\n", cfg.device);
    debug(" Capture delay: %d\n", cfg.captureDelay);
    debug("\n");

    if (!cfg.disableGPIO) {
        if (!(gpio = gpio_init_mmap()))
            goto error;

        gpio_init_leds(&cfg, gpio);
    }

    /* The video device takes quite a while show up in the Pi */
    if (!cfg.disableGPIO && cfg.pinPowerLED != 0)
        GPIO_SET(cfg.pinPowerLED);

    for (i = 0; i < 10000 && fd == -1; i++) {
        if ((fd = open(cfg.device, O_RDWR)) < 0) {
            if (i == 0) {
                debug("Waiting for %s.", cfg.device);
            } else {
                debug(".");
            }

            usleep(200*1000);
            if (toggle) {
                if (!cfg.disableGPIO && cfg.pinPowerLED != 0)
                    GPIO_SET(cfg.pinPowerLED);
            } else {
                if (!cfg.disableGPIO && cfg.pinPowerLED != 0)
                    GPIO_CLR(cfg.pinPowerLED);
            }

            toggle = !toggle;
        } else {
            if (i > 0) {
                debug("OK\n");
            }
        }
    }
    if (!cfg.disableGPIO && cfg.pinPowerLED != 0)
        GPIO_SET(cfg.pinPowerLED);

    if (fd < 0) {
        debug("Failed to open %s\n",
              argc > 1 ? argv[1] : "/dev/video0");
        goto error;
    }

    if (webcam_init_format(fd, &pix_format, &width, &height) < 0) {
        goto error;
    }

    if (!(buffer = webcam_init_mmap(fd))) {
        goto error;
    }

    if (start_streaming(fd) < 0) {
        goto error;
    }

    if (cfg.disableGPIO ||
        cfg.pinShutterButton == 0) {
        while (1) {
            debug("Capture one\n");
            if (!cfg.disableGPIO && cfg.pinShutterLED != 0)
                GPIO_SET(cfg.pinShutterLED);
            if (webcam_capture_image(fd, buffer, pix_format, width, height, &dir_index, &file_index))
                goto error;
            if (!cfg.disableGPIO && cfg.pinShutterLED != 0)
                GPIO_CLR(cfg.pinShutterLED);
            sleep(cfg.captureDelay);
        }
    } else {
        while (1) {
            if (GPIO_GET(cfg.pinShutterButton) == 0) {
                debug("Pressed\n");
                if (!debounced) {
                    usleep(1000ll * 500);
                    if (GPIO_GET(cfg.pinShutterButton) != 0) {
                        debug("Bounced\n");
                        continue;
                    }
                    debug("Debounced\n");
                    debounced = 1;
                }
                if (!cfg.disableGPIO && cfg.pinShutterLED != 0)
                    GPIO_SET(cfg.pinShutterLED);
                debug("Captured\n");
                if (webcam_capture_image(fd, buffer, pix_format, width, height, &dir_index, &file_index))
                    goto error;
                if (!cfg.disableGPIO && cfg.pinShutterLED != 0)
                    GPIO_CLR(cfg.pinShutterLED);
                sleep(cfg.captureDelay);
            } else {
                debug("Not pressed\n");
                debounced = 0;
                usleep(1000ll * 250);
            }
        }
    }

    if (!cfg.disableGPIO && cfg.pinPowerLED != 0)
        GPIO_CLR(cfg.pinPowerLED);

    if (stop_streaming(fd) < 0) {
        goto error;
    }
    close(fd);

    sync();
    umount("/data");
    reboot(RB_POWER_OFF);

    return 0;

 error:
    execl("/sh", "sh", NULL);
    return 1;
}

/*
 * Local variables:
 *  c-indent-level: 4
 *  c-basic-offset: 4
 *  indent-tabs-mode: nil
 *  tab-width: 8
 * End:
 */
